class Video < ApplicationRecord
  belongs_to :user

  has_many :activities, dependent: :destroy

  validates :title, presence: true
  validates :url, presence: true
  validates :poster, presence: true
  validates :description, presence: true

  scope :by_user, ->(user) { where(user_id: user) }

  def rating
    ratings = activities.pluck(:rating).compact

    return 0 if ratings.none?

    ratings.sum / ratings.size
  end

  def views
    activities.where(seen: true).count
  end
end
