class User < ApplicationRecord
  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :validatable,
         :registerable

  has_many :videos
  has_many :activities
end
