class Activity < ApplicationRecord
  belongs_to :user
  belongs_to :video, touch: true

  validates :user_id, presence: true
  validates :video_id, presence: true, uniqueness: { scope: :user_id }
end
