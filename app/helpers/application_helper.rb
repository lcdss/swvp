module ApplicationHelper
  def email_hash(value)
    Digest::MD5.hexdigest(value)
  end

  def app_name
    Rails.application.class.parent.to_s.upcase
  end

  def flash_messages
    flash.to_h.slice('alert', 'notice')
  end

  def flash_icons(type)
    case type
    when 'alert' then 'exclamation-circle'
    when 'notice' then 'check-circle'
    end
  end

  def flash_context_map(type)
    case type
    when 'alert' then 'danger'
    when 'notice' then 'success'
    end
  end
end
