class VideoPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def update?
    can_manage?
  end

  def destroy?
    can_manage?
  end

  private

  def can_manage?
    current_user.id == resource.user_id
  end
end
