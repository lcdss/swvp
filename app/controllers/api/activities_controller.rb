module Api
  class ActivitiesController < Controller
    def update
      @activity = Activity.find(params[:id])

      if activity_params.key?(:seen) && @activity.started_at.nil?
        @activity.started_at = Time.current
      end

      @activity.update(activity_params)
    end

    private

    def activity_params
      params
        .require(:activity)
        .permit(:rating, :seen, :started_at, :finished_at)
    end
  end
end
