module Backend
  class VideosController < CrudController
    before_action :authorize

    def index
      @videos = Video.by_user(current_user)
    end

    def show
      @video = Video.find(params[:id])

      @activity = Activity.find_or_create_by(
        video_id: @video.id,
        user_id: current_user.id
      )
    end

    def new
      @video = Video.new
    end

    def create
      @video = Video.new(video_params.merge(user_id: current_user.id))

      if @video.save
        return redirect_to @video, notice: 'Video created successfully.'
      end

      render :new
    end

    def edit
      @video = Video.find(params[:id])
    end

    def update
      @video = Video.find(params[:id])

      if @video.update(video_params)
        return redirect_to @video, notice: 'Video updated successfully.'
      end

      render :edit
    end

    def destroy
      @video = Video.find(params[:id])

      @video.destroy

      redirect_to videos_url, notice: 'Video deleted successfully.'
    end

    private

    def video_params
      params.require(:video).permit(:title, :url, :poster, :description)
    end
  end
end
