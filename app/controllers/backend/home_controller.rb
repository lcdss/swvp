module Backend
  class HomeController < Controller
    def index
      @videos = Video.includes(:user, :activities).order(updated_at: :desc)
    end
  end
end
