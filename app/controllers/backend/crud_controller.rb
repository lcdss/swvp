module Backend
  class CrudController < Controller
    helper_method :resource_name, :resource_class, :resource, :can?

    protected

    def resource_name
      controller_name.singularize
    end

    def resource_class
      resource_name.capitalize.constantize
    end

    def resource
      if instance_variable_defined?(:"@#{resource_name}")
        instance_variable_get(:"@#{resource_name}")
      else
        instance_variable_set("@#{resource_name}", resource_class.find_by(id: params[:id]))
      end
    end

    def resource_params
      if respond_to?("#{resource_name}_#{action_name}_params")
        send("#{resource_name}_#{action_name}_params")
      elsif respond_to?("#{resource_name}_params")
        send("#{resource_name}_params")
      else
        params.fetch(resource_name, {})
      end
    end

    def resource=(new_resource)
      instance_variable_set(:"@#{helpers.resource_name}", new_resource)
    end

    def can?(action)
      return true unless class_exists?(policy_name)

      policy.send("#{action}?")
    end

    def authorize
      return if can?(action_name)

      redirect_back(
        alert: 'You are not authorized to execute that action.',
        fallback_location: home_url
      )
    end

    def policy
      @policy ||= policy_class.new(current_user, helpers.resource)
    end

    def policy_class
      policy_name.constantize
    end

    def policy_name
      "#{helpers.resource_class.name}Policy"
    end

    private

    def class_exists?(class_name)
      klass = Module.const_get(class_name.to_s)

      klass.is_a?(Class)
    rescue NameError
      false
    end
  end
end
