class ApplicationController < ActionController::Base
  self.view_paths = Rails.root.join('resources', 'views')

  protect_from_forgery
end
