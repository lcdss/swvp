import Rails from 'rails-ujs'
import Turbolinks from 'turbolinks'

import 'bootstrap'
import 'video.js'

import { playVideoHandler, endVideoHandler } from './video'

Rails.start()
Turbolinks.start()

$(() => {
  $('video')
    .on('play', playVideoHandler)
    .on('ended', endVideoHandler)

  $('.rating input').on('change', (e) => {
    const $el = $(e.currentTarget)
    const $form = $el.closest('form')

    $form.submit()
  })
})
