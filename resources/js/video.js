export const playVideoHandler = (e) => {
  const data = e.target.dataset

  $.ajax({
    method: 'post',
    url: data.url,
    data: {
      _method: 'patch',
      activity: {
        seen: true
      }
    },
    success: () => {
      console.log('Video was seen')
    }
  })
}

export const endVideoHandler = (e) => {
  const data = e.target.dataset

  $.ajax({
    method: 'post',
    url: data.url,
    data: {
      _method: 'patch',
      activity: {
        finished_at: (new Date).toISOString()
      }
    },
    success: () => {
      console.log('Video has finished')
    }
  })
}
