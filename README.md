# SWVP - Simple Web Video Player
SWVP is a Rails web application to share videos with each others.

## How to Install
To install you will need have Docker and Docker Compose installed in your system and follow the steps below:

1. `git clone git@gitlab.com:lcdss/swvp.git`
2. `cd swvp`
3. `docker-compose up -d`
4. `docker-compose exec box sh`
5. `bundle && yarn`
6. `rake db:migrate`
7. **(optional)** To feed the database run `rake db:seed`
8. Run `bin/webpack` to compile the assets
9. Execute `rails s` and access the link http://localhost:3000 in your browser
