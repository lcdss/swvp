Rails.application.routes.draw do
  devise_for :users, module: 'auth', path: 'auth'

  scope module: :backend do
    get '/', to: 'home#index', as: :home

    resources :videos
  end

  namespace :api do
    resources :activities, only: :update
  end
end
