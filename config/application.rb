require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Swvp
  class Application < Rails::Application
    config.load_defaults 5.2

    config.time_zone = 'Brasilia'

    config.action_mailer.delivery_method = :smtp

    config.action_mailer.smtp_settings = {
      user_name: ENV['MAIL_USERNAME'],
      password: ENV['MAIL_PASSWORD'],
      address: ENV['MAIL_ADDRESS'],
      domain: ENV['MAIL_DOMAIN'],
      port: ENV['MAIL_PORT'],
      authentication: :cram_md5
    }
  end
end
