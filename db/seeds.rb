# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.transaction do
  (1..3).each do |index|
    user = User.create(
      name: "User #{index}",
      email: "user#{index}@example.com",
      password: '123456'
    )

    5.times do
      Video.create(
        title: Faker::Lorem.sentence(3),
        description: Faker::Lorem.paragraph,
        poster: Faker::LoremFlickr.image('1280x720'),
        url: 'https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8',
        user_id: user.id
      )
    end
  end
end
