class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.integer :rating
      t.boolean :seen, default: false
      t.timestamp :started_at
      t.timestamp :finished_at

      t.timestamps
    end

    add_reference :activities, :user, null: false, foreign_key: true
    add_reference :activities, :video, null: false, foreign_key: true

    add_index :activities, %i[user_id video_id], unique: true
  end
end
