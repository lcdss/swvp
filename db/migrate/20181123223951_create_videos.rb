class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string :title, null: false
      t.string :poster, null: false
      t.string :description, null: false
      t.string :url, null: false

      t.timestamps
    end

    add_reference :videos, :user, null: false, foreign_key: true
  end
end
