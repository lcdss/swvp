require 'application_system_test_case'

class VideosTest < ApplicationSystemTestCase
  test 'create a new video' do
    sign_in

    create_video

    assert page.has_content?('Video created successfully.')
    assert page.has_content?('My Video')
    assert page.has_content?('Show how the things are')
  end

  test 'edit an existent video' do
    sign_in

    visit videos_url

    create_video

    click_link 'Video Actions'
    click_link 'Edit'

    fill_in 'Title', with: 'My New Video Title'
    fill_in 'Description', with: 'Whatch the world breaking down'

    click_button 'Save'

    assert page.has_content?('Video updated successfully.')
    assert page.has_content?('My New Video Title')
    assert page.has_content?('Whatch the world breaking down')
  end

  test 'delete an existent video' do
    sign_in

    create_video

    click_link 'Video Actions'
    click_link 'Delete'

    assert page.has_no_content?('My Video')
    assert page.has_content?('SWVP Awesome Video')
    assert page.has_content?('Video deleted successfully.')
  end

  test 'rate an existent video' do
    sign_in

    create_video

    within '.rating' do
      find(:xpath, '//*/label[@for="activity_rating_5"]').click
    end

    visit home_url

    assert page.has_content?('Rating 5')
  end

  test 'check if the videos views are being accounted' do
    sign_in

    visit home_url

    assert page.has_content?('0 Views')
    assert page.has_no_content?('1 Views')

    # Click on User Amazing Video
    find(:xpath, '//*/div[2]/div/h3/a').click

    visit home_url

    assert page.has_content?('1 Views')
  end

  test 'try to edit an unauthorized video' do
    sign_in

    visit home_url

    # Click on SWVP Awesome Video
    find(:xpath, '//*/div[2]/div/h3/a').click

    assert page.has_no_link?('Video Actions')

    visit edit_video_path(videos(:unknown))

    assert page.has_content?('You are not authorized to execute that action.')
  end

  def create_video
    video = videos(:swvp)

    visit videos_url

    click_link 'New'

    fill_in 'Title', with: 'My Video'
    fill_in 'Url', with: video.url
    fill_in 'Poster', with: video.poster
    fill_in 'Description', with: 'Show how the things are'

    click_button 'Save'
  end
end
