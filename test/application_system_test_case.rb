require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :chrome, screen_size: [1400, 1400]

  include Warden::Test::Helpers

  def sign_in
    login_as(current_user, scope: :user)
  end

  def current_user
    users(:swvp)
  end
end
